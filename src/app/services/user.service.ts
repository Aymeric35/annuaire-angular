import { Injectable } from '@angular/core';
import axios from 'axios';
import { User } from 'src/app/models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  searchText = '';
  public USERS: any = [];

  public async addUser() {
    this.searchText = '';
    const response = await axios.get("https://randomuser.me/api/");
    try {
      let fetchedUser = response.data.results[0];
      let user: User = {
        firstname: fetchedUser.name.first,
        lastname: fetchedUser.name.last,
        age: fetchedUser.dob.age,
        picture: fetchedUser.picture.large,
        country: fetchedUser.location.country,
        email: fetchedUser.email
      }
      this.USERS.push(user);
    } catch (error) {
      console.log(error);
    }
  }

  public removeUserFromList(user: User) {
    this.USERS = this.USERS.filter((u: { email: string; }) => u.email !== user.email);
    console.log(this.USERS);
  }

  constructor() { }
}
