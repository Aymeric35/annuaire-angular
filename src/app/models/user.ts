export interface User {
    firstname: string,
    lastname: string,
    age: string,
    picture: any,
    country: string,
    email: string
}